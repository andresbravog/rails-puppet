# Puppet Rails

Provision a rails app with pupper

## Requirements

* Vagrant

```
brew cask install vagrant
```

* Virtualbox

```
brew cask install virtualbox
```

## Buld the base box

```
cd tools/box-builder
make build
```

## run the VM

```
vagrant up
```
