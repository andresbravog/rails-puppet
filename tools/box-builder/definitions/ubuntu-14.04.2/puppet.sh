GEM=/opt/ruby/bin/gem

adduser --system --group --home /var/lib/puppet puppet
$GEM install puppet -v 3.5.1 --no-ri --no-rdoc
