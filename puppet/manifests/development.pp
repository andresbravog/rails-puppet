stage { 'req-install': before => Stage['rbenv-install'] }
stage { 'rbenv-install': before => Stage['rails-app-create'] }
stage { 'rails-app-create': before => Stage['main'] }

class requirements {
  group { "puppet": ensure => "present", }
  exec { "apt-update":
    command => "/usr/bin/apt-get -y update",
    onlyif => "/bin/sh -c '[ ! -f /var/cache/apt/pkgcache.bin ] || /usr/bin/find /etc/apt/* -cnewer /var/cache/apt/pkgcache.bin | /bin/grep . > /dev/null'"
  }
  package {
    ["mysql-client", "mysql-server", "libmysqlclient-dev", "nodejs"]:
      ensure => installed, require => Exec['apt-update']
  }
}

class rbenv::installed {
  class { 'rbenv': }
  rbenv::plugin { [ 'sstephenson/rbenv-vars', 'sstephenson/ruby-build' ]: }
  rbenv::build { '2.0.0-p598': global => true }
  rbenv::gem { 'passenger': ruby_version => '2.0.0-p598', version => '5.0.8'}
}

class nginx::installed {
  class { 'nginx':
      passenger_root   => '/usr/local/rbenv/versions/2.0.0-p598/lib/ruby/gems/2.0.0/gems/passenger-5.0.8',
      passenger_ruby   => '/usr/local/rbenv/shims/ruby'
  }

  nginx::vhost { 'rails-puppet.dev':
      port => '80',
      rails => true,
      root => '/var/www/rails/public'
  }
}

class rails::app::created {
  $app_dirs = [ "/var/www", "/var/www/rails" ]
  file { $app_dirs:
      ensure => "directory",
      owner  => "vagrant",
      group  => "www-data",
      mode   => 775,
  }
  exec {"add vagrant membership to www-data":
    unless => "/bin/grep -q 'www-data\\S*vagrant' /etc/group",
    command => "/usr/sbin/usermod -aG www-data vagrant",
  }
  exec { "allow-bitbucket-origin":
    command  => "/bin/echo -e 'Host bitbucket.org\n\tStrictHostKeyChecking no\n' >> /home/vagrant/.ssh/config",
    user => 'vagrant',
    unless => "/bin/grep -qFx 'Host bitbucket.org' '/home/vagrant/.ssh/config'"
  }
  exec { "clone-rails-app":
    command  =>  "/usr/bin/git clone git@bitbucket.org:andresbravog/rails-backbone.git /var/www/rails",
    user => 'vagrant',
    require => Exec["allow-bitbucket-origin"],
    creates => "/var/www/rails/Gemfile"
  }
  exec { "bundle-rails-app":
    cwd  => "/var/www/rails/",
    command  =>  "/usr/local/rbenv/shims/bundle install",
    user => 'vagrant',
    require => Exec["clone-rails-app"],
  }
  exec { "db-setup-rails-app":
    environment => 'RAILS_ENV=production',
    cwd  => "/var/www/rails/",
    command  =>  "/usr/local/rbenv/shims/bundle exec rake db:setup",
    user => 'vagrant',
    require => Exec["bundle-rails-app"],
  }
  exec {"ensure-owners-rails-app":
    command => "/bin/chown -R  vagrant:www-data /var/www/rails",
  }
  exec { "precompile-assests-rails-app":
    environment => 'RAILS_ENV=production',
    cwd  => "/var/www/rails/",
    command  =>  "/usr/local/rbenv/shims/bundle exec rake assets:precompile",
    user => 'vagrant',
    require => Exec["bundle-rails-app","ensure-owners-rails-app"],
  }
}

class doinstall {
  class { requirements:, stage => "req-install" }
  class { rbenv::installed:, stage => "rbenv-install" }
  class { rails::app::created:, stage => "rails-app-create" }
  include nginx::installed
}

include doinstall
